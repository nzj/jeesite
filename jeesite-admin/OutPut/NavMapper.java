//Powered By ZSCAT, Since 2014 - 2020

package com.zs.pig.oa.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.oa.api.model.Nav;


/**
 * 
 * @author zs 2016-5-24 21:51:40
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的oa
 */
public interface NavMapper extends Mapper<Nav>{

}
