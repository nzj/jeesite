//Powered By ZSCAT, Since 2014 - 2020

package com.zs.pig.blog.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.blog.api.model.BlogTemplate;


/**
 * 
 * @author zs 2016-5-24 21:51:40
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的blog
 */
public interface BlogTemplateMapper extends Mapper<BlogTemplate>{

}
