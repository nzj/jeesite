/**
 * Copyright &copy; 2012-2016 <a href="http://git.oschina.net/wocadi/jeesite">JeeSite</a> All rights reserved.
 */
package com.zscat.cms.dao;

import com.zscat.cms.entity.Site;
import com.zscat.common.persistence.CrudDao;
import com.zscat.common.persistence.annotation.MyBatisDao;

/**
 * 站点DAO接口
 * @author ThinkGem
 * @version 2013-8-23
 */
@MyBatisDao
public interface SiteDao extends CrudDao<Site> {

}
