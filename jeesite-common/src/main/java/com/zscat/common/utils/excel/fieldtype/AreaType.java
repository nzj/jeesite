/**
 * Copyright &copy; 2012-2016 <a href="http://git.oschina.net/wocadi/jeesite">JeeSite</a> All rights reserved.
 */
package com.zscat.common.utils.excel.fieldtype;

import com.zscat.common.persistence.sys.Area;

/**
 * 字段类型转换
 * @author ThinkGem
 * @version 2013-03-10
 */
public class AreaType {

	/**
	 * 获取对象值（导入）
	 */
	public static Object getValue(String val) {
		
		return null;
	}

	/**
	 * 获取对象值（导出）
	 */
	public static String setValue(Object val) {
		if (val != null && ((Area)val).getName() != null){
			return ((Area)val).getName();
		}
		return "";
	}
}
